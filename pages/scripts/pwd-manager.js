"use strict";


function mirroredModify(parser) {
    return fallback => {
        return modificationExec => {
            return (targetElement, propName) => {
                const init = parser(targetElement[propName]) ?? fallback;
                const reflectedLocalSave = (key, val) => {
                    localStorage.setItem(key, val);
                    return val;
                };
                return targetElement[propName] = reflectedLocalSave(targetElement.id, modificationExec(init));
            };
        };
    };
}


const darkModeName = "Nox";
const lightModeName = "Lumos";
const lumToggle = document.querySelector("#lumination-toggle");
function setElPropFromStorage(el, propName, fallback) {
    return el[propName] = localStorage.getItem(el.id) ?? fallback;
}
if (setElPropFromStorage(lumToggle, "innerText", lightModeName) == darkModeName) {
    document.body.classList.toggle("light-mode");
}
const toggleLum = () => {
    const darkBiasedMirrorXBase = mirroredModify(x => x && x.length > 0 ? x : null)(darkModeName);
    const handleClass = passThrough => {
        document.body.classList.toggle("light-mode");
        return passThrough;
    };
    darkBiasedMirrorXBase(x => handleClass(x) == lightModeName ? darkModeName : lightModeName)(lumToggle, "innerText");
};
lumToggle.addEventListener("click", () => toggleLum());

function writeToClipboard(text, feedbackElement = null) {
    navigator.clipboard.writeText(text).then(
        () => {
            if (feedbackElement) {
                const oldText = feedbackElement.innerText;
                feedbackElement.innerText = "Wrote to clipboard";
                feedbackElement.classList.remove("error");

                setTimeout(() => {
                    feedbackElement.innerText = oldText;
                }, 3000)
            }
        },
        (e) => {
            console.log(e);
            if (feedbackElement) {
                const oldText = feedbackElement.innerText;
                feedbackElement.innerText = "Clipboard write failed";
                feedbackElement.classList.add("error");

                setTimeout(() => {
                    feedbackElement.innerText = oldText;
                    feedbackElement.classList.remove("error");
                }, 3000)
            }
        }
    );
}

class VisibilityWrapper {
    constructor(element) {
        this.element = element;
    }

    open() {
        this.element.classList.remove("hidden");
    }

    close() {
        this.element.classList.add("hidden");
    }
}

class ProtoTab {
    constructor(description, content) {
        this.description = description;
        this.content = content;
    }
}

class Tab extends ProtoTab {
    constructor(protoTab, element) {
        super(protoTab.description, protoTab.content);
        this.element = element;
    }

    createContentElement() {
        const applyProps = (kvs, target) => {
            for (const [key, value] of Object.entries(kvs)) {
                if (key == "el" || key == "children") continue;

                if (key == "classes") {
                    target.classList.add(...value);
                } else if (key == "listener") {
                    target.addEventListener(value.type, value.callback);
                } else if (value instanceof Function) {
                    target[key] = value();
                } else {
                    target[key] = value;
                }
            }
        };

        const base = document.createElement(this.content["el"]);
        applyProps(this.content, base);

        const addChildren = (parent, children) => {
            if (!children || children.length == 0) {
                return parent;
            }

            for (const c of children) {
                const cElement = document.createElement(c["el"]);
                applyProps(c, cElement);
                parent.appendChild(cElement);

                addChildren(cElement, c["children"]);
            }

            return parent;
        };

        return addChildren(base, this.content["children"]);
    }

    open() {
        this.element.firstElementChild.innerText = this.description;
        while (this.element.children.length > 2) {
            this.element.removeChild(this.element.lastElementChild);
        }
        this.element.appendChild(this.createContentElement());
        this.element.classList.remove("hidden");
    }
}

function ctrlTabs(backgroundElement, tabOpeners, targetElement, protoTabs) {
    const background = new VisibilityWrapper(backgroundElement);

    targetElement.children[1].addEventListener("click", (event) => {
        const tab = event.target.parentElement;
        tab.classList.add("hidden");
        tab.children[0].innerText = "";
        tab.removeChild(tab.lastElementChild);

        background.open();
        document.querySelector("footer").classList.remove("hidden");
    });

    const tabs = protoTabs.map(pt => new Tab(pt, targetElement));
    for (let i = 0; i < tabOpeners.length; i++) {
        tabOpeners[i].innerText = tabs[i].description;
        tabOpeners[i].addEventListener("click", () => {
            background.close();
            document.querySelector("footer").classList.add("hidden");
            tabs[i].open();
        });
    }
}


const intParser = x => {
    const res = parseInt(x);
    return isNaN(res) ? null : res;
};
const originIntMirrorXBase = mirroredModify(intParser)(0);

const backgroundElement = document.querySelector("#lock-art-wrapper");
const tabOpeners = document.querySelectorAll(".tab-opener");
const tabElement = document.querySelector("#tab");
const protoTabs = [
    new ProtoTab("Introduction", {
        el: "p",
        innerHTML: "<span class=\"omittable\">If you still use passwords that are simple/written down/used for many accounts, it's time to stop. </span>This site is a free & cross-platform password manager that you can use without having to install anything. There is no sync needed between multiple devices either, since your passwords are not saved at all. Instead, they are generated via a hash function<span class=\"omittable\"> (pbkdf2 with slightly more than 100 000 iterations). Only a minimal state consisting of a digit/digits is saved locally in the browser for convenience</span>. Make sure to choose a decent master password <a href=\"https://www.youtube.com/watch?v=3NjQ9b3pgIg\" target=\"_blank\">https://www.youtube.com/watch?v=3NjQ9b3pgIg</a> &#8212; ideally you should also use 2FA. This site is actually meant as a demo primarily and there are other, more mature deterministic password managers out there."
    }),
    new ProtoTab("Limitations", {
        el: "p",
        innerHTML: "Password rules like required special characters or passwords of a certain length are not supported. If you change a password<span class=\"omittable\"> (because you heard one of your sites was hacked or as a preventive measure)</span> via the state you will have to change all your passwords, unless you want to remember multiple states."
    }),
    new ProtoTab("Passwords", {
        el: "form",
        id: "pwd-gen-wrapper",
        children: [
            {
                el: "div",
                children: [
                    { el: "label", for: "master-password", innerText: "Master Password"},
                    { el: "input", type: "password", id: "master-password", classes: ["tab-bordered"]}
                ]
            },
            {
                el: "div",
                children: [
                    { el: "label", for: "site", innerText: "Site"},
                    { el: "input", type: "text", id: "site", classes: ["tab-bordered"]}
                ]
            },
            {
                el: "div",
                children: [
                    { el: "label", for: "state", innerText: "State"},
                    {
                        el: "div",
                        id: "state-wrapper",
                        children: [
                            { el: "button", id: "decrease-state", innerText: "-", classes: ["tab-bordered"],
                                listener: {type: "click", callback: event => {
                                        event.preventDefault();
                                        originIntMirrorXBase(x => --x < 0 ? 0 : x)(event.target.parentElement.children[1], "value");
                                    }
                                }
                            },
                            { el: "input", type: "text", readOnly: true, id: "state", value: () => localStorage.getItem("state") != null ? localStorage.getItem("state") : 0, classes: ["tab-bordered"] },
                            { el: "button", id: "increase-state", innerText: "+", classes: ["tab-bordered"],
                                listener: {type: "click", callback: event => {
                                        event.preventDefault();
                                        originIntMirrorXBase(x => ++x)(event.target.parentElement.children[1], "value");
                                    }
                                }
                            }
                        ]
                    }
                ]
            },
            {
                el: "div",
                children: [
                    { el: "label", for: "pwd-gen", innerText: "Generate"},
                    { el: "button", id: "pwd-gen", innerText: "Generate", classes: ["tab-bordered"],
                        listener: {type: "click", callback: event => {
                                event.preventDefault();

                                event.target.innerText = "Generating ..."
                                // give DOM some time to update before starting intensive task
                                setTimeout(() => {
                                    const generatedPassword = window.pbkdf2Sync(
                                        (document.querySelector("#master-password").value).normalize(),
                                        (document.querySelector("#site").value + document.querySelector("#state").value).normalize(),
                                        100251,
                                        8,
                                        "sha512"
                                    ).toString("hex");

                                    document.querySelector("#master-password").value = "";
                                    event.target.innerText = "Generate"
                                    event.target.parentElement.classList.add("hidden");
                                    event.target.parentElement.parentElement.lastElementChild.classList.remove("hidden");

                                    document.querySelector("#output").value = generatedPassword;

                                    setTimeout(() => {
                                        document.querySelector("#output").value = "";

                                        event.target.parentElement.parentElement.lastElementChild.classList.add("hidden");
                                        event.target.parentElement.classList.remove("hidden");
                                    }, 10000);
                                }, 100);
                            }
                        }
                    }
                ]
            },
            {
                el: "div",
                classes: ["hidden"],
                children: [
                    { el: "label", for: "output", innerText: "Output"},
                    {
                        el: "div",
                        id: "output-wrapper",
                        children: [
                            { el: "button", id: "cp-output", innerText: "c", classes: ["tab-bordered"],
                                listener: {type: "click", callback: event => {
                                        event.preventDefault();
                                        writeToClipboard(
                                            document.querySelector("#output").value,
                                            document.querySelector("#output").parentElement.parentElement.firstElementChild
                                        );
                                    }
                                }
                            },
                            { el: "input", type: "password", readOnly: true, id: "output", classes: ["tab-bordered"] },
                            { el: "button", id: "peek", innerText: "°", classes: ["tab-bordered"],
                                listener: {type: "click", callback: event => {
                                        event.preventDefault();
                                        event.target.parentElement.children[1].type = "text";
                                        setTimeout(() => event.target.parentElement.children[1].type = "password", 5000);
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }),
    new ProtoTab("More details", {
        el: "p",
        innerHTML: "You can change your passwords without having to change your master password by incrementing the state. If you have multiple accounts on the same site, you can include the usernames in the \"site\" input, e.g.: uname@sitename. Try to be consistent regarding what part of the URLs you use for the \"site\" input (domain only/including prefixes like subdomains) as you will otherwise have to remember different schemas regarding that input. The generation of the passwords happens exclusively locally. If you want to be safe you can download the source as a ZIP and use it locally in case of a Gitlab outage."
    })
];

ctrlTabs(backgroundElement, tabOpeners, tabElement, protoTabs);
