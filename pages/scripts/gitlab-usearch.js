"use strict";

// the token only has read-access, so hardcoding it like this is not a problem security-wise
// (and the access is scoped exclusively to the endpoints concering GitLab users)
const accessToken = "glpat-aQ2Rh4_bxz8GnL8JZLEj";
const baseUrl = "https://gitlab.com/api/v4";

function UserNotFoundException(message) {
    this.message = message;
}

function getUserInfo(userName) {
    return fetch(baseUrl + "/users?username=" + userName).then(res => res.json())
        .then(data => {
            if (data[0] == undefined) throw new UserNotFoundException("Not found");
            const {
                id,
                username: userName,
                name,
                avatar_url: profilePic,
                web_url: profileLink
            } = data[0];

            return fetch(baseUrl + "/users/" + id, {
                    headers: {"Authorization": " Bearer " + accessToken}
                }).then(res => res.json())
                .then(data => {
                    const {
                        followers,
                        following,
                        bio,
                        created_at: joiningDate
                    } = data;
                    return {
                        userName: userName,
                        name: name,
                        profilePic: profilePic,
                        profileLink: profileLink,
                        followerCount: followers,
                        followingCount: following,
                        bio: bio,
                        joiningDate: new Date(joiningDate)
                    };
                });

        });
}


const userName = document.querySelector("#username");
const name = document.querySelector("#name");
const profilePic = document.querySelector("#profile-pic");
const followerCount = document.querySelector("#follower-count");
const followingCount = document.querySelector("#following-count");
const bio = document.querySelector("#bio");
const joiningDate = document.querySelector("#joining-date");
const lightModePrimColor = "rgba(200, 200, 230, 0.7)";
const colorToggleIcon = document.querySelector("#color-toggle-icon");

function displayUserInfo(info) {
    name.innerText = info.name;
    userName.innerText = info.userName;
    userName.href = info.profileLink;
    profilePic.src = info.profilePic;
    followerCount.innerText = info.followerCount;
    followingCount.innerText = info.followingCount;
    info.bio.length > 0 ? bio.innerText = info.bio : bio.innerText = "This profile has no bio";
    let dateString = info.joiningDate.toDateString();
    joiningDate.innerText = "Joined " + dateString.substr(dateString.indexOf(" ") + 1);
}

function showSearchResults(event) {
    event.preventDefault();

    const feedbackElement = document.querySelector("#search-feedback");
    const resultElement = document.querySelector("#search-result");
    const inputElement = document.querySelector("#search-input");
    const input = inputElement.value;
    getUserInfo(input).then(info => {
        displayUserInfo(info);
        inputElement.classList.remove("input-shrunk");
        resultElement.style.visibility = "visible";
        feedbackElement.classList.remove("feedback-expanded");
        feedbackElement.classList.add("feedback-shrunk");
    }).catch(err => {
        inputElement.classList.add("input-shrunk");
        resultElement.style.visibility = "hidden";
        feedbackElement.classList.remove("feedback-shrunk");
        feedbackElement.classList.add("feedback-expanded");
        document.querySelector("#search-feedback-content").innerText = err.message;
    });
}

function isLightMode() {
    return getComputedStyle(document.body).getPropertyValue("--primaryColor").trim() == lightModePrimColor;
}

function getColorSchemeToggleText() {
    return isLightMode() ? "Dark" : "Light";
}

function changeColorScheme() {
    if (isLightMode()) {
        document.body.classList.add("dark-mode");
        document.body.classList.remove("light-mode");
        colorToggleIcon.classList.remove("fa-moon");
        colorToggleIcon.classList.add("fa-sun");
    } else {
        document.body.classList.add("light-mode");
        document.body.classList.remove("dark-mode");
        colorToggleIcon.classList.remove("fa-sun");
        colorToggleIcon.classList.add("fa-moon");
    }
    colorSchemeButton.innerText = getColorSchemeToggleText();
}

const searchButton = document.querySelector("#search-submit");
searchButton.addEventListener("click", showSearchResults, false);

const colorSchemeButton = document.querySelector("#color-scheme-toggle");
colorSchemeButton.addEventListener("click", changeColorScheme, false);

const text = getColorSchemeToggleText();
const initialCTogIcon = text == "Dark" ? "fa-moon" : "fa-sun";
colorToggleIcon.classList.add(initialCTogIcon);
colorSchemeButton.innerText = text;
