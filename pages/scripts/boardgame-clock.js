"use strict";

const iconIndex = 3;
const defaultInfo = "This is the generalization of a chess clock, usable for any number of players";
const overviewDiv = document.querySelector("#overview");
const detailsDiv = document.querySelector("#details");
const infoParagraph = document.querySelector("#info");
const firstOverviewToggle = document.querySelector("#overview-toggle-1");
const secondOverviewToggle = document.querySelector("#overview-toggle-2");
const playerNameInput = document.querySelector("#player-name-input");
const playerSubmitButton = document.querySelector("#player-name-submit");
const playerForm = document.querySelector("#player-form");
const clockList = document.querySelector("#clock-list");
const minutesInput = document.querySelector("#minutes");
const startButton = document.querySelector("#start");
const pauseButton = document.querySelector("#pause");
const nextButton = document.querySelector("#next");
const singleNextButton = document.querySelector("#single-next");
const continueButton = document.querySelector("#continue");
const resetButton = document.querySelector("#reset");
const singleTimerSection = document.querySelector("#single-timer");
const singlePlayerName = document.querySelector("#single-player-name");
const detailsClockList = document.querySelector("#details-clocklist");


displayInfo();
firstOverviewToggle.classList.add("hidden");
detailsDiv.classList.add("hidden");
let loadPreviousGameOnContinue = checkForSavedGameSnapshot();
let timers = [];
let activeTimerIndex = 0;
let colorIndex = -1;
let colors = ["#ff1818", "#39ff14", "#00b4d8", "#ffff33", "#8338ec", "#fdf0d5", "#fb5607", "#ff006e"];


function updateDetails() {
    singlePlayerName.innerText = getPlayerNames()[activeTimerIndex];

    for (let i = 0; i < detailsClockList.children.length; i++) {
        detailsClockList.children[i].classList.add("hidden");
    }

    detailsClockList.children[activeTimerIndex].classList.remove("hidden");

    singleTimerSection.style.backgroundColor = colors[activeTimerIndex % colors.length];
}


firstOverviewToggle.addEventListener("click", switchView);
secondOverviewToggle.addEventListener("click", switchView);
playerSubmitButton.addEventListener("click", submitPlayer);
startButton.addEventListener("click", startGame);
pauseButton.addEventListener("click", pauseGame);
nextButton.addEventListener("click", () => activateNextTimer(activeTimerIndex));
singleNextButton.addEventListener("click", () => {
    activateNextTimer(activeTimerIndex);
    updateDetails();

    hide([continueButton, resetButton]);
    show([pauseButton, nextButton]);
});
continueButton.addEventListener("click", continueGame);
resetButton.addEventListener("click", () => window.location.reload());


const timerCallback = el => {
    el.parentElement.children[0].classList.add("alert");
    el.classList.add("alert");
    styleIcon(el.parentElement.children[iconIndex], "lapsed");
    activateNextTimer(activeTimerIndex);

    updateDetails();
};

class Timer {

    // seconds: how long the countdown should be
    // granularity: how often the target element should be updated (in milliseconds)
    constructor(seconds, granularity, targetElements, callback = null) {
        this.seconds = seconds;
        this.granularity = granularity;
        this.targetElements = targetElements;
        this.callback = callback;
        this.running = false;
        this.remainingSeconds = null;
        this.intervalID = null;
    }

    countdown() {
        if (this.running) return;
        if (this.remainingSeconds != null) {
            if (this.remainingSeconds <= 0) {
                throw new Error("No time left");
            }

            this.seconds = this.remainingSeconds;
        }

        const start = Date.now();
        this.intervalID = setInterval(() => {
            this.remainingSeconds = this.seconds - Math.floor((Date.now() - start) / 1000);
            if (this.remainingSeconds <= 0) {
                this.pause();
                if (this.callback) this.callback(this.targetElements[0]);
            }
            this.updateTargets();
        }, this.granularity);
        this.running = true;
    }

    pause() {
        if (!this.running) return;
        clearInterval(this.intervalID);
        this.intervalID = null;
        this.running = false;
    }

    updateTargets() {
        const minutes = Math.floor(this.remainingSeconds / 60);
        const seconds = this.remainingSeconds % 60;
        this.targetElements.forEach(el => {
            el.innerText = (minutes < 10 ? "0" + minutes : minutes)
                + ":" + (seconds < 10 ? "0" + seconds : seconds);
        });
    }
}


function switchView() {
    overviewDiv.classList.toggle("hidden");
    detailsDiv.classList.toggle("hidden");

    if (!detailsDiv.classList.contains("hidden")) {
        updateDetails();
    }
}

function getNextColor() {
    colorIndex = colorIndex + 1 < colors.length ? colorIndex + 1 : 0;
    return colors[colorIndex];
}

function getPlayerNames() {
    return Array.from(clockList.children, child => child.children[0].children[0].innerText);
}

function submitPlayer(event) {
    event.preventDefault();

    if (!playerNameInput.value) {
        displayError("Please enter a name for the player");
        return;
    }
    const name = playerNameInput.value;
    if (!playerNameIsUnique(name)) {
        displayError("Please enter a unique name");
        return;
    }

    loadPreviousGameOnContinue = false;
    hide([continueButton]);

    displayInfo();
    const clockDiv = generateClock(name);
    clockList.appendChild(clockDiv);

    const altViewClock = document.createElement("li");
    detailsClockList.appendChild(altViewClock);

    playerNameInput.value = "";
}

function startGame(event) {
    event.preventDefault();

    if (clockList.children.length <= 1) {
        displayError("There must be at least two players to start a game");
        return;
    }

    let minutesPerPlayer = null;
    try {
        minutesPerPlayer = getPlayerMinutes();
    } catch (e) {
        displayError("Please enter a number for the amount of minutes per player");
        return;
    }

    displayInfo("Hint: you can click on a players name to directly jump to their turn");

    firstOverviewToggle.classList.remove("hidden");

    const overviewTargets = Array.from(clockList.children, child => child.children[2]);
    const detailTargets = Array.from(detailsClockList.children);
    for (let i = 0; i < overviewTargets.length; i++) {
        timers.push(new Timer(minutesPerPlayer*60, 100, [overviewTargets[i], detailTargets[i]], timerCallback));
    }

    const directTimerButtons = Array.from(clockList.children, child => child.children[0]);
    for (let i = 0; i < directTimerButtons.length; i++) {
        directTimerButtons[i].addEventListener("click", () => startClockDirectly(activeTimerIndex, i));
    }

    timers[activeTimerIndex].countdown();
    styleIcon(clockList.children[activeTimerIndex].children[iconIndex], "active");
    hide([minutesInput, startButton, playerNameInput, playerSubmitButton, playerForm]);
    show([pauseButton, nextButton]);
}

function pauseGame() {
    timers[activeTimerIndex].pause();
    const icon = clockList.children[activeTimerIndex].children[iconIndex];
    if (!icon.classList.contains("clock-lapsed")) styleIcon(icon, "paused");
    hide([pauseButton, nextButton]);
    show([continueButton, resetButton]);

    saveGameSnapshot();
}

function continueGame() {
    if (loadPreviousGameOnContinue) {
        displayInfo("Loading from snapshot");

        firstOverviewToggle.classList.remove("hidden");

        const players = JSON.parse(localStorage.getItem("boardgamePlayerNames"));
        players.forEach(player => {
            clockList.appendChild(generateClock(player));
            const altViewClock = document.createElement("li");
            detailsClockList.appendChild(altViewClock);
        });

        let loadedTimers = JSON.parse(localStorage.getItem("boardgameTimers"));
        for (let i = 0; i < loadedTimers.length; i++) {
            loadedTimers[i].targetElements = [
                clockList.children[i].children[2],
                detailsClockList.children[i]
            ];
            loadedTimers[i].callback = timerCallback;
        }
        timers = loadedTimers.map(timer => Object.assign(new Timer, timer));
        timers.forEach(timer => {
            if (timer.remainingSeconds != null && timer.remainingSeconds <= 0) {
                timer.targetElements[0].parentElement.children[0].classList.add("alert");
                timer.targetElements[0].classList.add("alert");
                styleIcon(timer.targetElements[0].parentElement.children[iconIndex], "lapsed");
            }
        });


        const directTimerButtons = Array.from(clockList.children, child => child.children[0]);
        for (let i = 0; i < directTimerButtons.length; i++) {
            directTimerButtons[i].addEventListener("click", () => startClockDirectly(activeTimerIndex, i));
        }

        hide([minutesInput, startButton, playerNameInput, playerSubmitButton, playerForm, continueButton]);
        show([pauseButton, nextButton]);

        activateNextTimer(timers.length - 1);
        loadPreviousGameOnContinue = false;
        displayInfo("Loaded the game from the stored snapshot");
        return;
    } else {
        displayInfo();
    }

    try {
        timers[activeTimerIndex].countdown();
        styleIcon(clockList.children[activeTimerIndex].children[iconIndex], "active");
        hide([continueButton, resetButton]);
        show([pauseButton, nextButton]);
    } catch {
        displayError("The game is over");
    }
}

function checkForSavedGameSnapshot() {
    const storedTimers = localStorage.getItem("boardgameTimers");
    const storedPlayers = localStorage.getItem("boardgamePlayerNames");
    if (storedTimers != undefined && storedPlayers != undefined) {
        displayInfo("Found a snapshot of a previous game, click Continue to load it");
        show([continueButton]);
        return true;
    }

    return false;
}

function saveGameSnapshot() {
    localStorage.setItem("boardgameTimers", JSON.stringify(timers));
    const names = Array.from(clockList.children, child => child.children[0].children[0].innerText);
    localStorage.setItem("boardgamePlayerNames", JSON.stringify(names));
    displayInfo("Locally saved a snapshot of the timers");
}

function getPlayerMinutes() {
    const result = parseInt(minutesInput.value);
    if (Number.isNaN(result)) throw new Error("Invalid input");
    return result;
}

function startClockDirectly(oldTimerIndex, nextTimerIndex) {
    const result = activateNextTimer(oldTimerIndex, nextTimerIndex);
    if (result != 1) {
        hide([continueButton, resetButton]);
        show([pauseButton, nextButton]);
    }
}

function activateNextTimer(oldTimerIndex, nextTimerIndex = null) {
    timers[oldTimerIndex].pause();
    const icon = clockList.children[oldTimerIndex].children[iconIndex];
    if (!icon.classList.contains("clock-lapsed")) styleIcon(icon, "paused");

    if (nextTimerIndex != null) {
        try {
            timers[nextTimerIndex].countdown();

            activeTimerIndex = nextTimerIndex;
            styleIcon(clockList.children[nextTimerIndex].children[iconIndex], "active");
            displayInfo();
        } catch (e) {
            displayError("The selected timer has run out of time");
            return 1;
        }

        return;
    }

    let timerIndex = oldTimerIndex;
    let tries = 0;
    while (tries <= timers.length) {
        tries++;
        timerIndex++;
        if (timerIndex >= timers.length) {
            timerIndex = 0;
        }

        try {
            timers[timerIndex].countdown();

            activeTimerIndex = timerIndex;
            styleIcon(clockList.children[timerIndex].children[iconIndex], "active");
            displayInfo();
            break;
        } catch (e) { }
    }
}

function playerNameIsUnique(playerName) {
    const whitespaceMatcher = /\s/g;
    const names = Array.from(clockList.children, child => child.children[0].children[0].innerText.replace(whitespaceMatcher, ""));
    return !names.includes(playerName.replace(whitespaceMatcher, ""));
}

function generateClock(playerName) {
    const clockDiv = document.createElement("div");
    clockDiv.classList.add("clock");

    const clockTrigger = document.createElement("button");
    const clockName = document.createElement("li");
    clockName.innerText = playerName;
    clockTrigger.appendChild(clockName);
    clockDiv.appendChild(clockTrigger);

    const colorDiv = document.createElement("div");
    colorDiv.classList.add("vertical-ribbon");
    colorDiv.style.backgroundColor = getNextColor();
    clockDiv.appendChild(colorDiv);

    const timeDiv = document.createElement("div");
    timeDiv.classList.add("time");
    clockDiv.appendChild(timeDiv);

    const icon = document.createElement("i");
    styleIcon(icon, "paused");
    clockDiv.appendChild(icon);

    return clockDiv;
}

function styleIcon(icon, selector) {
    icon.classList.remove(...icon.classList);
    if (selector == "paused") {
        icon.classList.add("fa-solid", "fa-pause", "clock-paused");
    } else if (selector == "active") {
        icon.classList.add("fa-solid", "fa-play", "clock-active");
    } else if (selector == "lapsed") {
        icon.classList.add("fa-solid", "fa-bell", "clock-lapsed", "alert");
    }
}

function displayInfo(info = defaultInfo) {
    infoParagraph.innerText = info;
    infoParagraph.classList.remove("error-message");
}

function displayError(info) {
    infoParagraph.innerText = info;
    infoParagraph.classList.add("error-message");
}

function hide(elements) {
    elements.forEach(element => element.classList.add("hidden"));
}

function show(elements) {
    elements.forEach(element => element.classList.remove("hidden"));
}
