"use strict";


const input = document.querySelector("input");
input.onblur = () => {
    setTimeout(() => input.focus(), 0);
};
const canvas = document.querySelector("canvas");
canvas.height = window.innerHeight;
canvas.width = window.innerWidth;
const maxCol = Math.floor(canvas.width/15) - 1;
const maxRow = Math.floor(canvas.height/15) - 1;
const fontSize = 15;
const chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!§$&/()?{[]}#@".split("");


class CommandError {
    constructor(message) {
        this.message = message;
    }
}
class SegmentationError {
    constructor(message) {
        this.message = message;
    }
}


const randInt = max => Math.floor(Math.random() * (max + 1));
function generateCorners(width, height) {
    const wMax = maxCol - width;
    const hMax = maxRow - height;
    const origin = [randInt(wMax), randInt(hMax)];
    return [origin, [width - 1 + origin[0], height - 1 + origin[1]]];
}
class Rectangle {
    constructor(corners) {
        [this.origin, this.dest] = Rectangle.alignCorners(corners);
    }

    contains(x, y) {
        return x >= this.origin[0] && x <= this.dest[0]
            && y >= this.origin[1] && y <= this.dest[1];
    }

    positions() {
        let pos = [];
        for (let x = this.origin[0]; x <= this.dest[0]; x++) {
            for (let y = this.origin[1]; y <= this.dest[1]; y++) {
                pos.push([x, y]);
            }
        }
        return pos;
    }

    equals(rect) {
        if (!rect) return false;
        const coordsEqual = (a, b) => {
            for (let i = 0; i < a.length; i++) {
                if (a[i] != b[i]) return false;
            }

            return true;
        };

        return coordsEqual(this.origin, rect.origin) && coordsEqual(this.dest, rect.dest);
    }

    overlaps(rect) {
        class AxisPath {
            constructor(origin, dest) {
                this.origin = origin;
                this.dest = dest;
            }
        }
        const separateAxes = rect => {
            let axes = [];
            for (let i = 0; i < rect.origin.length; i++) {
                axes.push(new AxisPath(rect.origin[i], rect.dest[i]));
            }
            return axes;
        };

        const zip = (arr0, arr1) => {
            let res = [];
            for (let i = 0; i < arr0.length; i++) {
                res.push([arr0[i], arr1[i]]);
            }
            return res;
        };

        const linesOverlap = (line0, line1) => {
            return line1.origin >= line0.origin && line1.origin <= line0.dest ||
                line1.dest <= line0.dest && line1.dest >= line0.origin;
        };

        return zip(separateAxes(this), separateAxes(rect)).filter(pair => linesOverlap(...pair)).length > this.origin.length - 1;
    }

    static alignCorners(corners) {
        const origin = [Math.min(corners[0][0], corners[1][0]), Math.min(corners[0][1], corners[1][1])];
        const dest = [Math.max(corners[0][0], corners[1][0]), Math.max(corners[0][1], corners[1][1])];
        return [origin, dest];
    }
}


class RectangleDecorations {
    constructor(corner, hSide, vSide) {
        this.corner = corner;
        this.hSide = hSide;
        this.vSide = vSide;
    }
}
const decorations = new RectangleDecorations("x", "|", "–");


let master;
let state;
let passwords = {};
function cachePassword(site, pwd) {
    passwords[site] = {data: pwd, time: Date.now()};
}
function deleteStalePasswords() {
    const num = Date.now();
    passwords = Object.fromEntries(
        Object.entries(passwords).filter(e => num - e[1].time <= 7000)
    );
}
setInterval(deleteStalePasswords, 1000);
const writePwdToClip = () => {
    navigator.clipboard.writeText(
        Object.entries(passwords).length == 1 ? Object.values(passwords)[0].data : JSON.stringify(passwords)
    );
};
const commands = [
    {name: "init", scheme: "<master password>", exec: pwd => {
        master = pwd;
        state = localStorage.getItem("state") ?? 0;
        return "Started new session";
    }},
    {name: "state", scheme: "<int>", exec: numStr => {
        localStorage.setItem("state", numStr);
        state = numStr;
        return "Set state";
    }},
    {name: "pwd", scheme: "<site>", exec: site => {
        if (!master || master.length == 0) throw new CommandError("No password provided");
        if (!site || site.length == 0) throw new CommandError("No site provided");
        return site;
    }},
    {name: "copy", scheme: "", exec: site => {
        if (Object.entries(passwords).length == 0) throw new CommandError("No passwords in cache");
        return "Wrote passwords to clip";
    }},
    {name: "quit", scheme: "", exec: () => {
        if (!master || master.length == 0) throw new CommandError("No session to quit");
        master = "";
        navigator.clipboard.writeText("");
        return "Quit session";
    }}
];


class CtxHull {
    constructor(ctx, fontSize, offsets) {
        ctx.font = fontSize + "px arial";
        this.core = ctx;
        this.fontSize = fontSize;
        [this.xShift, this.yShift] = offsets;
        this.writeHist = {};
    }

    sync(accom) {
        for (const [setting, value] of Object.entries(accom)) {
            this.core[setting] = value;
        }
    }

    mountProxy(excludedSegments, accoms) {
        const hull = this;
        return new Proxy(this.core, {
            get(target, prop, receiver) {
                if (prop == "switchAccom") {
                    return (index) => {
                        hull.sync(accoms[index]);
                    }
                }

                const value = target[prop];
                if (value instanceof Function) {
                    return (...args) => {
                        if (prop != "fillRect") {
                            if (!hull.verify(args, excludedSegments)) {
                                throw new SegmentationError("Detected a segment border violation");
                            }
                        }
                        if (prop == "fillText") {
                            hull.handleChronology(args);
                            args = hull.shift(args);
                        }
                        return value.bind(target)(...args);
                    }
                }

                return value;
            },

            set(...args) {
                throw {message: "Detected a set call to the ctx"};
            }
        });
    }

    verify(args, excludedSegments) {
        const coords = args.filter(x => typeof x == "number").slice(0, 2);
        for (let i = 0; i < excludedSegments.length; i++) {
            if (excludedSegments[i].contains(...coords)) return false;
        }
        return true;
    }

    shift(args) {
        let n = null;
        for (let i = 0; i < args.length; i++) {
            if (typeof args[i] == "number") {
                if (n === null) n = i;
                args[i] *= this.fontSize;
                if (n != null && i - n < 2) {
                    args[i] += (i - n) == 0 ? this.xShift : this.yShift;
                }
            }
        }

        return args;
    }

    handleChronology(args) {
        const [x, y] = args.filter(x => typeof x == "number").slice(0, 2);

        if (this.writeHist[x] === undefined) this.writeHist[x] = {};
        if (this.writeHist[x][y] !== undefined) {
            const style = this.core.fillStyle;
            this.core.fillStyle = "rgba(0, 0, 0, 1)";
            const shiftedArgs = this.shift([this.writeHist[x][y], x, y])
            for (let i = 0; i < 10; i++) {
                this.core.fillText(...shiftedArgs);
            }
            this.core.fillStyle = style;
        }

        this.writeHist[x][y] = args[0];
    }
}


class CallDirective {
    constructor(scheme, ...args) {
        this.scheme = scheme;
        this.args = args;
    }
}
class ClientSpace {
    constructor(accom, buff, ephemeral, segment = null) {
        this.ctxAccom = accom instanceof Array ? accom : [accom];
        this.reqBuffer = buff;
        this.ephemeral = ephemeral;
        this.segment = segment;
        this.active = true;
    }

    detach() {
        this.active = false;
    }
}
class SyncMediator {
    constructor(ctx, fontSize, offsets) {
        this.conn = new CtxHull(ctx, fontSize, offsets);
        this.clientSpaces = [];
    }

    // throws if segment overlaps
    stackClient(ctxAccom, decay, segment = null) {
        const space = this.createValidatedSpace(ctxAccom, decay, segment);
        this.clientSpaces.push(space);
        return this.spawnCtxHandle(space);
    }

    // throws if segment overlaps
    queueClient(ctxAccom, decay, segment = null) {
        const space = this.createValidatedSpace(ctxAccom, decay, segment);
        this.clientSpaces.unshift(space);
        return this.spawnCtxHandle(space);
    }

    // vomits sometimes
    operate() {
        for (const space of this.clientSpaces) {
            if (space.reqBuffer.length == 0) continue;

            this.conn.sync(space.ctxAccom[0]);
            const proxy = this.conn.mountProxy(this.getOtherSegments(space), space.ctxAccom);
            try {
                space.reqBuffer[0].scheme(proxy, ...space.reqBuffer[0].args);
            } catch (e) { } finally { 
                space.reqBuffer.shift();
            }
        }

        this.unwind();
    }

    createValidatedSpace(ctxAccom, decay, segment = null) {
        if (this.detectCollision(segment)) throw new SegmentationError("The requested segment could not be granted");
        return new ClientSpace(ctxAccom, [], decay, segment);
    }

    detectCollision(requestedSeg) {
        if (requestedSeg) {
            const segments = this.clientSpaces.map(s => s.segment).filter(s => s);
            for (const seg of segments) {
                if (seg.overlaps(requestedSeg)) return true;
            }
        }

        return false;
    }

    spawnCtxHandle(clientSpace) {
        const requestWrite = (scheme, ...args) => {
            clientSpace.reqBuffer.push(new CallDirective(scheme, ...args));
        };

        const drop = () => {
            clientSpace.detach();
        };

        return [requestWrite, drop];
    }

    unwind() {
        for (const space of this.clientSpaces) {
            if (space.ephemeral) space.reqBuffer.length = 0;
        }

        this.clientSpaces = this.clientSpaces.filter(space => space.active || !space.active && space.reqBuffer.length > 0);
    }

    getOtherSegments(space) {
        return this.clientSpaces.map(s => s.segment).filter(s => s && !s.equals(space.segment))
    }
}


class PromptWindow extends Rectangle {
    constructor(corners, ctxHandle, rectangleDecorations) {
        super(corners);
        this.width = this.dest[0] - this.origin[0];
        this.height = this.dest[1] - this.origin[1];
        this.requestCtx = ctxHandle[0];
        this.dropHandle = ctxHandle[1];
        this.borderChars = rectangleDecorations;
        this.blinked = false;
        
        this.requestCtx(ctx => {
            this.positions().forEach(pos => ctx.fillText("", pos[0], pos[1]));
        });
    }

    displayText(text, interactive, err = false) {
        this.requestCtx(ctx => {
            this.drawBorder(ctx);

            const coords = [this.origin[0] + 2, Math.floor((this.origin[1] + this.dest[1])/2)];
            if (err) {
                ctx.switchAccom(1);
            } else if (interactive) {
                text = ">" + text;
                if (this.blinked) {
                    text = text + "_";
                }
                this.blinked = !this.blinked;
            }

            ctx.fillText(text, ...coords);
        });
    }

    drawBorder(ctx) {
        ctx.switchAccom(0);

        ctx.fillText(this.borderChars.corner, this.origin[0], this.origin[1]);
        ctx.fillText(this.borderChars.corner, this.dest[0], this.dest[1]);
        ctx.fillText(this.borderChars.corner, this.origin[0], this.dest[1]);
        ctx.fillText(this.borderChars.corner, this.dest[0], this.origin[1]);

        for (let x = this.origin[0] + 1; x < this.dest[0]; x++) {
            ctx.fillText(this.borderChars.hSide, x, this.dest[1]);
            ctx.fillText(this.borderChars.hSide, x, this.origin[1]);
        }

        for (let y = this.origin[1] + 1; y < this.dest[1]; y++) {
            ctx.fillText(this.borderChars.vSide, this.origin[0], y);
            ctx.fillText(this.borderChars.vSide, this.dest[0], y);
        }
    }

    detach() {
        this.displayText("", false);
        this.dropHandle();
    }
}


function dropHint(requestWrite) {
    const choices = commands.map(c => c.name + " " + c.scheme);
    const command = choices[randInt(choices.length - 1)].split("");
    const y = randInt(maxRow);
    const baseX = randInt(maxCol - command.length);
    for (const [i, c] of command.entries()) {
        requestWrite(ctx => {
            ctx.fillText(c, i + baseX, y);
        });
        setTimeout(() => {
            requestWrite(ctx => {
                ctx.fillText("", i + baseX, y);
            });
        }, 7000);
    }
}
function fade(requestWrite) {
    requestWrite(ctx => {
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    });
}
function generateNoise(requestWrite) {
    requestWrite(ctx => {
        for (let i = 0; i < 3; i++) {
            const x = randInt(maxCol);
            const y = randInt(maxRow);
            ctx.fillText(chars[randInt(chars.length - 1)], x, y);
        }
    });
}


function detectMob() {
    const toMatch = [
        /Android/i,
        /webOS/i,
        /iPhone/i,
        /iPad/i,
        /iPod/i,
        /BlackBerry/i,
        /Windows Phone/i
    ];

    return toMatch.some((toMatchItem) => {
        return navigator.userAgent.match(toMatchItem);
    });
}


if (!detectMob()) {
    const syncer = new SyncMediator(canvas.getContext("2d"), fontSize, [0, 11]);
    const corners = generateCorners(20, 5);
    const accoms = [
        {"fillStyle": "rgba(255, 255, 255, 1)"},
        {"fillStyle": "rgba(255, 100, 100, 1)"},
        {"fillStyle": "rgba(0, 0, 0, 1)"}
    ];
    let activePrompt = new PromptWindow(
        corners, syncer.stackClient(accoms, false, new Rectangle(corners)), decorations
    );
    const createNewPrompt = () => {
        let tries = 0;
        let corners;
        let newPrompt;
        while (!newPrompt && tries < 20) {
            corners = generateCorners(20, 5);
            try {
                newPrompt = new PromptWindow(corners, syncer.stackClient(accoms, false, new Rectangle(corners)), decorations);
            } catch (e) {
                tries++;
            }
        }

        return newPrompt;
    };
    input.addEventListener("keydown", (e) => {
        if (e.keyCode != 13) return;

        let newPrompt = createNewPrompt();
        if (!newPrompt) return;
        const promptToDetach = activePrompt;
        activePrompt = newPrompt;

        const args = input.value;
        input.value = "";

        let message;
        let words;
        let err;
        try {
            words = args.split(" ");
            if (words.length > 2) throw new CommandError("Invalid number of inputs");
            const command = commands.filter(c => c.name == words[0])[0];
            if (!command) throw new CommandError("Command not recognized");
            message = command.exec(words[1]);
        } catch (e) {
            err = e;
        }


        if (err || words[0] != "pwd") {
            promptToDetach.displayText(message ?? err.message, false, !!err);
            setTimeout(() => {
                promptToDetach.detach();
            }, 7000);
            return;
        }

        window.pbkdf2(
            master.normalize(),
            (message + state).normalize(),
            100251,
            8,
            "sha512",
            (err, key) => {
                if (err) throw err;
                const pwd = key.toString("hex");
                promptToDetach.displayText(pwd, false);
                cachePassword(words[1], pwd);
                writePwdToClip();
                setTimeout(() => {
                    promptToDetach.detach();
                }, 7000);
            }
        );
    });
    input.oninput = e => {
        const maxLength = 23;
        if (input.value.length > maxLength) input.value = input.value.substring(0, maxLength);
        activePrompt.displayText(input.value, true);
    };
    setInterval(() => activePrompt.displayText(input.value, true), 500);
    const [hintRequest, ] = syncer.queueClient({"fillStyle": "rgba(255, 100, 100, 1)"}, false);
    setInterval(() => dropHint(hintRequest), 37000);
    const [fadeRequest, ] = syncer.queueClient({"fillStyle": "rgba(0, 0, 0, 0.02)"}, false);
    setInterval(() => fade(fadeRequest), 500);
    const [noiseRequest, ] = syncer.queueClient({"fillStyle": "rgba(255, 255, 255, 1)"}, true);
    setInterval(() => generateNoise(noiseRequest), 79);
    setInterval(syncer.operate.bind(syncer), 15);
}
