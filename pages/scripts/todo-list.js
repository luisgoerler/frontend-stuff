"use strict";


const todoInput = document.querySelector("#todo-input");
const todoSubmitButton = document.querySelector("#todo-button");
const todoList = document.querySelector("#todo-list");


document.addEventListener("DOMContentLoaded", displayStoredTodos);
todoSubmitButton.addEventListener("click", addTodo);
todoList.addEventListener("click", modifyListState);


function addTodo(event) {
    event.preventDefault();

    saveTodoLocally(todoInput.value.trim());

    const todoDiv = generateTodoDiv(todoInput.value.trim(), false);
    todoList.appendChild(todoDiv);

    // clear input field after submit
    todoInput.value = "";
}

function modifyListState(event) {
    const item = event.target;
    const todoDiv = item.parentElement;
    if (item.classList[0] == "trash-btn") {
        removeStoredTodo(todoDiv.children[0].innerText);
        todoDiv.classList.add("fall");
        todoDiv.addEventListener("transitionend", () => todoDiv.remove());
    } else if (item.classList[0] == "complete-btn") {
        todoDiv.classList.toggle("completed");
        updateTodoStatus(todoDiv.children[0].innerText, todoDiv.classList.contains("completed"));
    }
}

function displayStoredTodos() {
    const todos = getStoredTodos();
    todos.forEach((todo) => {
        const todoDiv = generateTodoDiv(todo.text, todo.done);
        todoList.appendChild(todoDiv);
    });
}

function saveTodoLocally(text) {
    const todo = {text: text, done: false};
    let todos = getStoredTodos();
    todos.push(todo);
    overwriteStoredTodos(todos);
}

function updateTodoStatus(text, isDone) {
    let todos = getStoredTodos();
    const index = getTodoIndex(todos, text);
    if (index == null) return;
    todos[index].done = isDone;
    overwriteStoredTodos(todos);
}

function removeStoredTodo(text) {
    let todos = getStoredTodos();
    const index = getTodoIndex(todos, text);
    if (index == null) return;
    todos.splice(index, 1);
    overwriteStoredTodos(todos);
}

function getTodoIndex(todos, text) {
    const index = todos.map(todo => todo.text).indexOf(text);
    return index == -1 ? null : index;
}

function overwriteStoredTodos(todos) {
    localStorage.setItem("todos", JSON.stringify(todos));
}

function getStoredTodos() {
    const storedData = localStorage.getItem("todos");
    return storedData ? JSON.parse(storedData) : [];
}

function generateTodoDiv(text, isDone) {
    const todoDiv = document.createElement("div");
    todoDiv.classList.add("todo-item");
    if (isDone) todoDiv.classList.add("completed");

    const todoLiElement = document.createElement("li");
    todoLiElement.innerText = text;
    todoDiv.appendChild(todoLiElement);

    const completeButton = document.createElement("button");
    completeButton.innerHTML = "<i class=\"fa-solid fa-check\"></i>";
    completeButton.classList.add("complete-btn");
    todoDiv.appendChild(completeButton);

    const trashButton = document.createElement("button");
    trashButton.innerHTML = "<i class=\"fa-solid fa-trash\"></i>";
    trashButton.classList.add("trash-btn");
    todoDiv.appendChild(trashButton);

    return todoDiv;
}
