# Front-end stuff

As of writing this, I am a Front-end noob. That should change! This is a place where I collect interesting Front-end code.
There might be code here that I copied and just experimented with => no guarantee that I wrote the code (unlike my other repos)

## Viewing the pages

You can view the pages at https://luisgoerler.gitlab.io/frontend-stuff/
